#!make

PROJECT_ID = 30363503

# include gitignored .env file if present
-include .env
#export

all: lint

.PHONY: lint
lint:
	@echo "[] Linting gitlab-ci files"

	@if test -z "${CI_TOKEN}"; then \
		echo "Dev Error. CI_TOKEN is not defined."; \
		echo "See .env.dist file how to get around that."; \
		false; \
	 fi

	@curl "https://gitlab.com/api/v4/projects/${PROJECT_ID}/ci/lint" \
		--fail \
		--silent \
		--header "Content-Type: application/json" \
		--header "Authorization: Bearer ${CI_TOKEN}" \
		--data '{"content": "$(shell cat auto-devops/Auto-DevOps.gitlab-ci.yml | yq eval -o=j -I=0 | sed 's/"/\\"/g')"}'
